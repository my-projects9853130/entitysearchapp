const mongoose = require("mongoose");

const CsvSchema = new mongoose.Schema({
	fileName: {
		type: String,
		required: [true, "File name is required!"]
	},
	// content: {
	// 	type: String,
	// 	required: [true, "Csv content is required!"]
	// },
	status: {
		type: String,
		default: "Pending Review"
	},
	dateSubmitted: {
		type: Date,
		default: new Date()
	},
	totalRows: {
		type: Number,
		required: [true, "Csv total rows required!"]
	},
	duplicates: {
		type: Number,
		required: [true, "Csv matched duplicates required!"]
	},
	matches: {
		type: Number,
		required: [true, "Csv number of matches required!"]
	}
});

let Csv = mongoose.model('csvs', CsvSchema);
module.exports = Csv;
