const mongoose = require("mongoose");

const entitySchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Entity name is required!"]
	},
	type: {
		type: String,
		required: [true, "Entity type is required!"]
	},
	hits: {
		type: Number,
		default: 0,
		validate: {
    		validator: Number.isInteger,
    		message: '{VALUE} is not an integer value.'
  		}
	},
	curated: {
		type: Number,
		default: 0,
		validate: {
    		validator: Number.isInteger,
    		message: '{VALUE} is not an integer value.'
  		}
	}
});

let Entity = mongoose.model('entities', entitySchema);
module.exports = Entity;
