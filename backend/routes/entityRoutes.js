const entityControllers = require("../controllers/entityControllers.js");
const express = require("express");

const router = express.Router();
router.post("/entities", entityControllers.getEntities);

module.exports = router;
