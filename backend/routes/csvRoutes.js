const csvControllers = require("../controllers/csvControllers.js");
const express = require("express");

const router = express.Router();
router.post("/submitCsv", csvControllers.submitCsv);
router.patch("/updateStatus", csvControllers.updateCsvStatus);
router.get("/getSubmissions", csvControllers.getSubmissions);

module.exports = router;
