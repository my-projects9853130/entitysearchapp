const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const entityRoutes = require("./routes/entityRoutes.js");
const csvRoutes = require("./routes/csvRoutes.js");

const port = 4001;
const app = express();

mongoose.connect("mongodb+srv://admin:admin@batch288repollo.jojjyon.mongodb.net/EntitySearchApp?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true
});
mongoose.connection.on("error", console.error.bind(console, "Error! Can't connect to the database."));
mongoose.connection.once("open", () => console.log("We are connected to the database.")); 

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/entity", entityRoutes);
app.use("/csv", csvRoutes);

app.listen(port, () => console.log(`The server is now running at port ${port}.`));
