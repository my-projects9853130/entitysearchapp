const Csv = require("../models/Csvs.js");

module.exports.submitCsv = (request, response) => {
	//console.log(request.body);
	new Csv({
		fileName: request.body.fileName,
		//content: request.body.content,
		totalRows: request.body.totalRows,
		duplicates: request.body.duplicates,
		matches: request.body.matches
	}).save()
	.then(saved => response.send(true))
	.catch(error => response.send(false));
}

module.exports.updateCsvStatus = (request, response) => {
	Csv.findByIdAndUpdate(request.body.csvId, {status: request.body.status})
	.then(result => response.send(true))
	.catch(error => response.send(false));
}

module.exports.getSubmissions = (request, response) => {
	Csv.find({}).then(result => {
		if (result.length > 0) {
			return response.send(result);
		}
		else {
			return response.send(false);
		}
	}).catch(error => response.send(false));
}
