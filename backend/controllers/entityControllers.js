const Entity = require("../models/Entities.js");

module.exports.getEntities = async (request, response) => {
	const data = request.body.csvData;

	const promisedEntities = data.map(async company => {
		const companyName = company.CompanyName;
		let stringSearches = companyName.split(' ');
		let resultArray = [];
		let counter = 0;
		
		while (counter < stringSearches.length && resultArray.length == 0) {
			let stringSearch = stringSearches[counter].replaceAll(',', '').replaceAll("'", '').replaceAll('.', '');

			await Entity.find({"name":
			{
				"$regex": stringSearch,
				"$options": "i"
			}
			}).then(result => {
				if (result.length > 0) {
					resultArray = filterData(result, companyName);
				}
			}).catch(error => console.log(error));

			counter++;
		}
		
		return resultArray;
	});

	const entities = await Promise.all(promisedEntities);

	if (entities.length > 0) {
		const resultFinal = {entities: entities}
		//console.log(JSON.stringify(resultFinal));
		response.send(resultFinal);
	}
	else {
		response.send(false);
	}
}

function filterData(result, companyName) {
	let resultArray = [];

	const companyNameComp = companyName.toLowerCase()
		.replaceAll(' ', '').replaceAll(',', '').replaceAll("'", '').replaceAll('.', '');

	result.forEach(dataResult => {
		if (dataResult.type.toLowerCase() == "company") {

			const dataResultName = dataResult.name.toLowerCase()
				.replaceAll(' ', '').replaceAll(',', '').replaceAll("'", '').replaceAll('.', '');

			if (dataResultName.includes(companyNameComp))
			{
				resultArray.push(dataResult);
			}
			else if (companyNameComp.includes(dataResultName))
			{
				resultArray.push(dataResult);
			}
		}
	});

	return resultArray;
}
